/*
 * Author:  David Robert Nadeau
 * Site:    http://NadeauSoftware.com/
 * License: Creative Commons Attribution 3.0 Unported License
 *          http://creativecommons.org/licenses/by/3.0/deed.en_US
 */

#include "memory.h"

char *
get_time(char *buf, size_t buf_size) {
  time_t t;
  struct tm *tmp;
  char fmt[] = "%Y-%m-%d %H:%M:%S";

  t = time(NULL);
  tmp = localtime(&t);

  strftime(buf, buf_size, fmt, tmp);

  return buf;
}

/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t
getPeakRSS(void) {
  struct rusage rusage;
  getrusage(RUSAGE_SELF, &rusage);
  return (size_t)(rusage.ru_maxrss * 1024L);
}

/**
 * Returns the current resident set size (physical memory use) measured
 * in bytes, or zero if the value cannot be determined on this OS.
 */
size_t
getCurrentRSS(void) {
  long rss = 0L;
  FILE* fp = NULL;
  if((fp = fopen( "/proc/self/statm", "r" )) == NULL)
    return (size_t)0L;    /* Can't open? */

  if(fscanf(fp, "%*s%ld", &rss) != 1) {
    fclose(fp);
    return (size_t)0L;    /* Can't read? */
  }
  fclose(fp);

  return (size_t)rss * (size_t)sysconf(_SC_PAGESIZE);
}
