#include "keypair.h"

static uint32_t
keypair_hash(const char *s) {
  uint32_t h = (uint32_t) *s;
  if(h) for(++s; *s; ++s) h = (h << 5) - h + (uint32_t) *s;
  return h;
}

keypair_t *
keypair_new(uint32_t total_keys, uint8_t total_threads) {
  keypair_t *keypair;

  keypair = malloc(sizeof(keypair_t));

  keypair->keys = calloc(total_keys, sizeof(char *));
  keypair->hashes = calloc(total_keys, sizeof(uint8_t));
  keypair->total_threads = total_threads;
  keypair->total_keys = total_keys;

  return keypair;
}

uint8_t
keypair_should_get(keypair_t *keypair, uint32_t i, uint8_t my_rank) {
  /* We add one because we use 0 to represent not ready */
  return keypair->hashes[i] == 1 + my_rank && keypair->keys[i][0] != '\0';
}

void
keypair_add_key(keypair_t *keypair, uint32_t position, char *key) {
  keypair->keys[position] = strdup(key);

  /* We add one because we use 0 to represent not ready */
  keypair->hashes[position] = 1 + (keypair_hash(key) % keypair->total_threads);
}

uint8_t
keypair_is_ready(keypair_t *keypair, uint32_t position) {
  return keypair->keys[position] && keypair->hashes[position];
}

char *
keypair_get_key(keypair_t *keypair, uint32_t position) {
  return keypair->keys[position];
}

void
keypair_free(keypair_t *keypair) {
  size_t i;

  for(i = 0;i < keypair->total_keys; i++)
    free(keypair->keys[i]);

  free(keypair->keys);
  free(keypair->hashes);
  free(keypair);
}
