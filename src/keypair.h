#ifndef _KEYPAIR_H_
#define _KEYPAIR_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct keypair_t {
  char **keys;
  uint8_t *hashes;
  uint8_t total_threads;
  uint32_t total_keys;
} keypair_t;

keypair_t *keypair_new(uint32_t total_keys, uint8_t total_threads);
void keypair_add_key(keypair_t *keypair, uint32_t i, char *key);
char *keypair_get_key(keypair_t *keypair, uint32_t i);
uint8_t keypair_is_ready(keypair_t *keypair, uint32_t i);
uint8_t keypair_should_get(keypair_t *keypair, uint32_t i, uint8_t my_rank);
void keypair_free(keypair_t *keypair);
#endif
