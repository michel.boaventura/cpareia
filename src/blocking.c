#include "blocking.h"

void
blocking_thread_params_free(blocking_thread_params_t *params) {
  free(params);
}

void
blocking_generate_keys(project_t *project, uint32_t id) {
  size_t i, j, size, conj_size;
  conjunction_t *conjunction;
  part_t *part;
  record_t *record;
  char buffer[5], key[1024], *field;

  record = array_get(project->d0->records, id);

  conj_size = array_size(project->conjunctions);

  for(i = 0; i < conj_size; i++) {
    conjunction = array_get(project->conjunctions, i);
    key[0] = '\0';

    for(j = 0; j < array_size(conjunction->parts); j++) {
      part = array_get(conjunction->parts, j);
      field = record_get_field(record, part->field);

      if(!field[0]) {
        key[0] = '\0';
        break;
      }

      if(!part->transform) {
        strcat(key, field);
      } else if(!strcmp(part->transform, "brsoundex")) {
        brsoundex(field, buffer, record_field_size(record, part->field), 5);
        strcat(key, buffer);
      } else if(!strcmp(part->transform, "first-word")) {
        size = record_field_size(record, part->field);
        strncat(key, field, first_word(field, size));
      }
      else {
        handle_error("Unknown transformation");
      }
    }
    keypair_add_key(project->keypair, id * conj_size + i, key);
  }
}

void *
blocking_generate_all_keys(void *data) {
  size_t i, size;
  int rank, total_ranks;
  double prop;
  project_t *project;
  blocking_thread_params_t *param;

  param = data;

  rank = param->rank;
  project = param->project;
  total_ranks = param->total_ranks;
  size = project->d0->nrows;

  for(i = rank; i < size; i += total_ranks) {
    while(!array_get(project->d0->records, i)) sleep(1);
    if(!(i % 1000000) && !rank) {
      prop = 100.0 * i / size;
      printf("Registros blocados: %lu/%lu (%2.2f%%)\n", i, size, prop);
    }
    blocking_generate_keys(project, i);
  }

  if(!rank)
    printf("Registros blocados: %lu/%lu (%2.2f%%)\n", size, size, 100.0);

  blocking_thread_params_free(param);

  return NULL;
}

void *
blocking_generate_blocks(void *data) {
  size_t i;
  blocking_thread_params_t *param = data;
  uint32_t nmemb, rank, conj_size;
  project_t *project;
  keypair_t *keypair;

  project = param->project;
  block_t *block = project->blocks[param->rank];
  keypair = param->project->keypair;
  rank = param->rank;
  conj_size = array_size(project->conjunctions);

  nmemb = project->d0->nrows * conj_size;

  for(i = 0; i < nmemb; i++) {
    while(!keypair_is_ready(keypair, i)) sleep(1);
    if(keypair_should_get(keypair, i, rank))
      block_insert(block, keypair_get_key(keypair, i), i / conj_size);
  }
  blocking_thread_params_free(param);
  return NULL;
}

pthread_t **
blocking_async(project_t *project) {
  pthread_t **threads;
  blocking_thread_params_t *param;
  uint32_t i, j, max_threads;

  max_threads = project->args->key_threads + project->args->block_threads;

  threads = malloc(sizeof(pthread_t *) * max_threads);

  for(i = 0; i < max_threads; i++)
    threads[i] = malloc(sizeof(pthread_t));

  for(i = 0; i < project->args->key_threads; i++) {
    param = malloc(sizeof(blocking_thread_params_t));
    param->project = project;
    param->rank = i;
    param->total_ranks = project->args->key_threads;

    pthread_create(threads[i], NULL, blocking_generate_all_keys, param);
  }

  for(i = 0; i < project->args->block_threads; i++) {
    param = malloc(sizeof(blocking_thread_params_t));
    param->project = project;
    param->rank = i;
    param->total_ranks = project->args->block_threads;
    j = i + project->args->key_threads;

    pthread_create(threads[j], NULL, blocking_generate_blocks, param);
  }

  return threads;
}
