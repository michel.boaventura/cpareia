#include "project.h"

project_t *
project_new() {
  project_t *project;

  project = malloc(sizeof(project_t));

  project->conjunctions = array_new(1);

  project->blocks = NULL;

  project->args = args_new();

  project->output     = NULL;
  project->task       = NULL;
  project->name       = NULL;
  project->d0         = NULL;
  project->works      = NULL;
  project->classifier = NULL;
  project->keypair    = NULL;

  return project;
}

char *
project_get_attr(ezxml_t xml, const char *attr, const char *tag, int required) {
  const char *str;

  if(!(str = ezxml_attr(xml, attr)))  {
    if(required)
      handle_error("Missing attribute '%s' into tag <%s>", attr, tag);
    else
      return NULL;
  }

  return strdup(str);
}

void
project_print(project_t *project) {
  size_t i;

  printf("Project: %s\n", project->name);
  printf("Task: %s\n", project->task);
  for(i = 0; i < array_size(project->conjunctions); i++) {
    printf("Conjunction %d:\n", (int) i);
    conjunction_print(array_get(project->conjunctions, i));
  }

  printf("\nClassifier\n");
  classifier_print(project->classifier);

  printf("\nD0:\n");
  database_print(project->d0);

  printf("\nOutput:\n");
  output_print(project->output);
}

void
project_free(project_t *project) {
  size_t i;

  /* Output depends on everything else, so we need to lock here first */
  output_free(project->output);

  free(project->task);
  free(project->name);

  for(i = 0; i < array_size(project->conjunctions); i++)
    conjunction_free(array_get(project->conjunctions, i));

  for(i = 0; i < project->args->block_threads; i++)
    block_free(project->blocks[i]);

  database_free(project->d0);
  keypair_free(project->keypair);

  free(project->blocks);

  array_free(project->works);

  array_free(project->conjunctions);
  classifier_free(project->classifier);
  args_free(project->args);

  free(project);
}

void
project_add_conjunction(project_t *project, conjunction_t *conjunction) {
  array_push(project->conjunctions, conjunction);
}

void
project_parse_classifier(project_t *project, ezxml_t xml) {
  ezxml_t ch, root;
  char *match_min, *not_match_max, *m, *u, *missing, *function, *child_name;
  char *field, *default_weight;
  char *min_to_match, *use_weight_table, *frequency_table;
  comparator_type type;
  comparator_t *comparator;

  ch = ezxml_child(xml, "classifier");

  match_min     = project_get_attr(ch, "match-min-result", "classifier", 1);
  not_match_max = project_get_attr(ch, "not-match-max-result", "classifier", 1);

  project->classifier = classifier_new(atoi(match_min), atoi(not_match_max));

  root = ch = ch->child;
  type = inv;

  while(root) {
    child_name = ezxml_name(ch);

    if(!child_name)
      handle_error("The <classifier> tag doesn't have any children");
    else if(!strcmp(child_name, "exact-string-comparator"))
      type = exact;
    else if(!strcmp(child_name, "approx-string-comparator"))
      type = approx;
    else if(!strcmp(child_name, "br-city-comparator"))
      type = brcity;
    else {
      continue;
    }

    m                = project_get_attr(ch, "m", child_name, 0);
    u                = project_get_attr(ch, "u", child_name, 0);
    missing          = project_get_attr(ch, "missing", child_name, 0);
    min_to_match     = project_get_attr(ch, "minValueToBeMatch", child_name, 0);
    use_weight_table = project_get_attr(ch, "use-weight-table", child_name, 0);
    frequency_table  = project_get_attr(ch, "frequency-table", child_name, 0);
    function         = project_get_attr(ch, "function", child_name, 0);
    field            = project_get_attr(ch, "field", child_name, 0);
    default_weight   = project_get_attr(ch, "default-weight", child_name, 0);

    if(ch->next)
      ch = ch->next;
    else
      root = ch = root->sibling;

    comparator = comparator_new(
        type,
        use_weight_table ? (!strcmp(use_weight_table, "true") ? 1 : 0) : 0,
        m ? atof(m) : 0,
        u ? atof(u) : 0,
        missing ? atof(missing) : 0,
        project_get_field_id(project, field),
        frequency_table,
        function,
        min_to_match ? atof(min_to_match) : 0,
        default_weight ? atof(default_weight) : 0);

    if(!comparator)
      handle_error("Field %s\n", field);
    else
      array_push(project->classifier->comparators, comparator);

    free(m);
    free(u);
    free(field);
    free(missing);
    free(min_to_match);
    free(default_weight);
    free(use_weight_table);
  }

  free(match_min);
  free(not_match_max);
}

void
project_parse_datasource(project_t *project, ezxml_t xml) {
  ezxml_t child, tmp;
  char *filename, *separator, *rows;
  database_t *database;
  int i, nfields = 0;
  char sep;
  size_t nrows;

  child = ezxml_get(xml, "data-sources", 0, "data-source", -1);

  rows      = project_get_attr(child, "rows", "data-source", 1);
  filename  = project_get_attr(child, "file", "data-source", 1);
  separator = project_get_attr(child, "field-separator", "data-source", 1);

  child = ezxml_get(child, "fields", 0, "field", -1);

  sep = !strcmp(separator, "\\t") ? '\t' : *separator;

  if((tmp = child) == NULL)
    handle_error("No field has been found on <data-source>");

  do {
    nfields++;
  } while((tmp = ezxml_next(tmp)));

  nrows = strtoull((char *) rows, NULL, 10);

  database = database_new(filename, nfields, nrows, sep);

  for(i = 0; i < nfields; i++, child = ezxml_next(child))
    database->fields[i] = project_get_attr(child, "name", "field", 1);

  project->d0 = database;

  free(separator);
  free(rows);
}

void
project_parse_project(project_t *project, ezxml_t xml) {
  if(strcmp(xml->name, "project"))
    handle_error("Missing tag <project>. Found %s\n", ezxml_name(xml));

  project->name = project_get_attr(xml, "name", "project", 1);
  project->task = project_get_attr(xml, "task", "project", 1);

  if(strcmp(project->task, "deduplication"))
    handle_error("We only support 'deduplication' task right now");
}

void
project_parse_output(project_t *project, ezxml_t xml) {
  ezxml_t child;
  char *filename, *min, *max;

  child = ezxml_child(xml, "output");

  min      = project_get_attr(child, "min", "output", 1);
  max      = project_get_attr(child, "max", "output", 1);
  filename = project_get_attr(child, "file", "output", 1);

  project->output = output_new(filename, atof(min), atof(max));

  free(min);
  free(max);
}

void
project_parse_conjunctions(project_t *project, ezxml_t xml) {
  ezxml_t child, part;
  char *field_name, *transform, *str_size;
  conjunction_t *conjunction;
  int field, size;

  child = ezxml_get(xml, "blocking", 0, "conjunction", -1);

  do {
    conjunction = conjunction_new(1);
    part = ezxml_child(child, "part");
    do {
      str_size   = project_get_attr(part, "size", "part", 0);
      transform  = project_get_attr(part, "transform", "part", 0);
      field_name = project_get_attr(part, "field-name", "part", 1);

      size = str_size ? atoi(str_size) : 0;
      field = project_get_field_id(project, field_name);
      assert(field != -1);

      conjunction_add_part(conjunction, field, transform, size);

      free(field_name);
      free(transform);
      free(str_size);
    } while((part = ezxml_next(part)));
    project_add_conjunction(project, conjunction);
  } while((child = ezxml_next(child)));
}

void
project_parse(project_t *project, char *file_name) {
  ezxml_t xml;
  uint32_t i, nmemb;

  if((xml = ezxml_parse_file(file_name)) == NULL)
    handle_error("Unable to parse XML file %s\n", file_name);

  project_parse_project(project, xml);
  project_parse_datasource(project, xml);
  project_parse_conjunctions(project, xml);
  project_parse_classifier(project, xml);
  project_parse_output(project, xml);

  nmemb = project->d0->nrows * array_size(project->conjunctions);

  project->keypair = keypair_new(nmemb, project->args->block_threads);

  project->blocks = malloc(sizeof(block_t *) * project->args->block_threads);

  for(i = 0; i < project->args->block_threads; i++)
    project->blocks[i] = block_new();

  ezxml_free(xml);
}

int
project_get_field_id(project_t *project, char *field_name) {
  unsigned int i;
  int field;

  field = -1;

  for(i = 0; i < project->d0->nfields; i++) {
    if(!strcmp(field_name, project->d0->fields[i])) {
      field = i;
      break;
    }
  }

  if(field == -1) {
    handle_error("Field '%s' not found\n", field_name);
  }

  return field;
}
