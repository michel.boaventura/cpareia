#include "args.h"

args_t *
args_new(void) {
  args_t *args;

  args = malloc(sizeof(args_t));
  args->project_file  = NULL;

  return args;
}

void
args_free(args_t *args) {
  free(args->project_file);
  free(args);
}

void
args_print_usage() {
  handle_error("Usage: ./cpareia -p project_file [-t max_threads]\n");
}

void
args_calc_threads(args_t *args, int max_threads) {
  /* We need at least 3 threads */
  if(max_threads < 3) {
    max_threads = sysconf(_SC_NPROCESSORS_ONLN);
    printf("max_threads precisa ser maior que 3.\n");
    printf("Usando número de cores como padrão (%d).\n", max_threads);
  }

  args->max_threads   = max_threads;
  args->key_threads   = (max_threads - 1) / 2;
  args->read_threads  = 1;
  args->block_threads = args->max_threads - args->key_threads;
}

void
args_parse(args_t *args, int argc, char *argv[]) {
  int c, max_threads;
  struct option options[] = {
    {"project",   required_argument, 0, 'p'},
    {"threads",   optional_argument, 0, 't'},
    {0, 0, 0, 0}
  };
  static char optstr[] = "p:t:";
  max_threads = 0;

  while ((c = getopt_long(argc, argv, optstr, options, NULL)) != -1) {
    switch(c) {
      case 'p':
        args->project_file = strdup(optarg);
        check_file(args->project_file);
        break;
      case 't':
        max_threads = atoi(optarg);
        break;
      case '?':
      default:
        args_print_usage();
    }
  }
  if(!args->project_file)
    args_print_usage();

  args_calc_threads(args, max_threads);
}
