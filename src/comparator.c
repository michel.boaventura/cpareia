#include "comparator.h"

work_t *
work_new(uint_array_t *uint_array, int start, int step) {
  work_t *work;

  work = malloc(sizeof(work_t));

  work->array = uint_array;
  work->start = start;
  work->step = step;

  return work;
}

void
work_free(work_t *work) {
  free(work);
}

double
compare(comparator_t *comparator, record_t *r, record_t *s) {
  int match, result, field;
  double score, freq1, freq2;
  array_t *array1, *array2;
  char *f1, *f2;
  int s1, s2;

  field = comparator->field;

  f1 = record_get_field(r, field);
  f2 = record_get_field(s, field);

  if(!f1[0] || !f2[0])
    return comparator->missing;

  s1 = record_field_size(r, field);
  s2 = record_field_size(s, field);

  match = 0;
  score = 0;

  if(comparator->type == exact)
    match = (s1 == s2) && !memcmp(f1, f2, s1);
  else if(comparator->type == approx)
    match = comparator->fn(f1, f2, s1, s2) >= comparator->min_value_to_be_match;
  else if(comparator->type == brcity) {
    result = br_city(f1, f2, s1, s2);
    match = 0;

    if(result == 0)
      match = -1;
    else if(result == 1)
      match = result >= comparator->min_value_to_be_match;
    else if(result == 2)
      match = result >= comparator->min_value_to_be_match;
  }

  if(match == -1)
    score = comparator->log2_m_u + comparator->log2_1m_1u;
  else if(match == 0)
    score = comparator->log2_1m_1u;
  else if(match == 1){
    if(comparator->use_weight_table) {
      /*
       * Implementar tabela de peso
       * score = inverse_freq_f1 | inverse_freq_f2 | default_weight
       */
      array1 = hash_get(comparator->frequency_table, f1);
      array2 = hash_get(comparator->frequency_table, f2);

      freq1 = array1 ? *(double *)array_get(array1, 0) : 0;
      freq2 = array2 ? *(double *)array_get(array2, 0) : 0;

      /* We need to get the smalest inverse frequency, since it means
       * this element is more frequent.
       */
      if(!freq1 && !freq2) {
        score = comparator->default_weight;
      } else {
        score = MIN(freq1, freq2);
      }

    } else {
      score = comparator->log2_m_u;
    }
  }

  return score;
}

double
compare_all(
    classifier_t *classifier,
    record_t *r,
    record_t *s,
    double *scores) {
  size_t i;
  double score;

  score = 0;

  for(i = 0; i < array_size(classifier->comparators); i++) {
    scores[i] = compare(array_get(classifier->comparators, i), r, s);
    score += scores[i];
  }

  return score;
}

void
compare_block(work_t *work, project_t *project, int id, uint64_t *num_comp) {
  size_t i, j, size, classes;
  record_t *r1, *r2;
  double *scores;
  double score;
  char status;

  size = array_size(work->array);
  classes = array_size(project->classifier->comparators);
  scores = malloc(sizeof(double) * classes);

  for(i = work->start; i < size - 1; i += work->step) {
    r1 = array_get(project->d0->records, array_get(work->array, i));
    for(j = i + 1; j < size; j++) {
      r2 = array_get(project->d0->records, array_get(work->array, j));
      score = compare_all(project->classifier, r1, r2, scores);
      (*num_comp)++;

      if(score <= project->classifier->not_match_max)
        status = 'N';
      else if(score >= project->classifier->match_min)
        status = 'Y';
      else
        status = '?';

      if(between(score, project->output->min, project->output->max)) {
        output_write(
            project->output,
            record_get_id(r1),
            record_get_id(r2),
            status,
            score,
            scores,
            classes,
            id);
      }
    }
  }
  free(scores);
  work_free(work);
}

void *
compare_block_void(void *data) {
  unsigned long int i, size, step;
  int id, last_thread;
  uint64_t num_comp;
  double prop;
  comparator_pthread_params_t *par;
  project_t *project;

  par = data;

  project = par->project;
  id = par->id;

  size = (unsigned long int) array_size(project->works);

  last_thread = id == par->num_threads - 1;

  step = size < 100 ? size : size * 0.0001;

  num_comp = 0;

  for(i = id; i < size; i += par->num_threads) {
    if(last_thread && !(i % step)) {
      prop = 100.0 * i / size;
      printf("Blocos processados: %lu/%lu (%2.2f%%)\n", i, size, prop);
    }
    compare_block(array_get(project->works, i), project, id, &num_comp);
  }

  if(last_thread)
    printf("Blocos processados: %lu/%lu (%2.2f%%)\n", size, size, 100.0);

  printf("Thread %d, num_comp %lun", id, num_comp);

  free(data);

  return NULL;
}

void
comparator_get_block(const char *key, uint_array_t *array, void *proj) {
  work_t *work;
  project_t *project;
  float prop;
  int i;
  size_t size;
  (void) key;

  project = (project_t *) proj;
  size = uint_array_size(array);

  prop = size / project->blocks_mean_size;
  prop = prop > 2 ? prop : 1;

  for(i = 0; i < prop; i++) {
    work = work_new(array, i, prop);
    array_push(project->works, work);
  }
}

int
comparator_calc_sum(const char *key, uint_array_t *array, void *ac) {
  size_t size;
  float *acc;
  (void) key;

  acc = (float *) ac;
  size = uint_array_size(array);

  if(size == 1) {
    return 1;
  }

  *acc += size;

  return 0;
}

pthread_t **
comparator_run_async(project_t *project) {
  pthread_t **threads;
  comparator_pthread_params_t *param;
  uint32_t i, size = 0;
  float acc = 0;

  threads = malloc(sizeof(pthread_t *) * project->args->max_threads);

  for(i = 0; i < project->args->block_threads; i++)
    size += block_size(project->blocks[i]);

  printf("Calculando trabalho médio e removendo blocos únicos\n");
  printf("Quantidade de blocos: %d\n", size);

  size = 0;

  for(i = 0; i < project->args->block_threads; i++) {
    block_foreach_remove(project->blocks[i], comparator_calc_sum, &acc);
    size += block_size(project->blocks[i]);
  }

  if(!size)
    handle_error("Erro. Nenhum bloco restou para ser comparado\n");

  size = MAX(size, project->args->max_threads);

  project->blocks_mean_size = acc / size;

  printf("Trabalho médio: %.0f registros\n", project->blocks_mean_size);
  printf("Novos blocos: %d\n", size);

  project->works = array_new(size);

  printf("Dividindo e compartilhando blocos para a comparação\n");

  for(i = 0; i < project->args->block_threads; i++)
    block_foreach(project->blocks[i], comparator_get_block, project);

  printf("Todos os blocos foram alocados\nComeçando comparação em si\n");

  output_open_files(project->output, project->args->max_threads);

  for(i = 0; i < project->args->max_threads; i++) {
    threads[i] = malloc(sizeof(pthread_t));
    param = malloc(sizeof(comparator_pthread_params_t));
    param->project = project;
    param->id = i;
    param->num_threads = project->args->max_threads;
    pthread_create(threads[i], NULL, compare_block_void, param);
  }

  return threads;
}
