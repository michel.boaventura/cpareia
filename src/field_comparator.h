#ifndef _FIELD_COMPARATOR_H_
#define _FIELD_COMPARATOR_H_

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void brsoundex(char *, char *, size_t, size_t);
int first_word(char *, size_t);
double winkler(char *, char *, int, int);
double sift4(char *, char *, int, int);
int br_city(char *, char *, int, int);

#endif
