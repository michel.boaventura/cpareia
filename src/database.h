#ifndef _DATABASE_H_
#define _DATABASE_H_

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <pthread.h>

#ifdef HAVE_SSE2
#include <emmintrin.h>
#endif

#include "record.h"
#include "errors.h"
#include "array.h"

typedef struct database_t {
  char sep;
  uint16_t nfields;
  uint32_t nrows;
  uint64_t filesize;
  array_t *records;
  char **fields, *filename, *buf;
} database_t;

database_t *database_new(char *filename, int nfields, size_t nrows, char sep);
void database_free(database_t *database);
void database_read(database_t *database);
pthread_t *database_read_async(database_t *database);
void database_print(database_t *database);
int8_t find_byte(char const *s, char ch);

#endif
