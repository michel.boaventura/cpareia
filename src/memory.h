#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

size_t getPeakRSS(void);
size_t getCurrentRSS(void);
char *get_time(char *, size_t);

#endif
