#ifndef _PROJECT_H_
#define _PROJECT_H_

#include "database.h"
#include "conjunction.h"
#include "classifier.h"
#include "block.h"
#include "output.h"
#include "args.h"
#include "ezxml.h"
#include "keypair.h"

typedef struct project_t {
  char *name, *task;
  database_t *d0;
  array_t *conjunctions, *works;
  block_t **blocks;
  classifier_t *classifier;
  output_t *output;
  float blocks_mean_size;
  args_t *args;
  keypair_t *keypair;
} project_t;

project_t *project_new();
void project_print(project_t *);
void project_free(project_t *);
void project_parse(project_t *, char *);
int project_get_field_id(project_t *, char*);

#endif
