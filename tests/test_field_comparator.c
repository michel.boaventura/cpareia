#include "test_field_comparator.h"

void
test_brsoundex() {
  int size = 5;
  char buffer1[size], buffer2[size];

  char str1[] = "silva";
  char str2[] = "SILVA";

  char str3[] = "santos";
  char str4[] = "SANTOS";

  brsoundex(str1, buffer1, sizeof(str1), size);
  brsoundex(str2, buffer2, sizeof(str2), size);
  assert_string_equal(buffer1, "s410");
  assert_string_equal(buffer1, buffer2);

  brsoundex(str3, buffer1, sizeof(str1), size);
  brsoundex(str4, buffer2, sizeof(str2), size);
  assert_string_equal(buffer1, "s532");
  assert_string_equal(buffer1, buffer2);
}

void
test_first_word() {
  char line1[] = "foo123 bar123";
  char line2[] = "foo123bar123";

  assert_int_equal(first_word(line1, sizeof(line1)), 6);
  assert_int_equal(first_word(line2, sizeof(line2)), sizeof(line2));
}

void
test_br_city() {
  assert_int_equal(br_city("54321", "12345", 5, 5), 0);
  assert_int_equal(br_city("12345", "12345", 5, 5), 1);
  assert_int_equal(br_city("12345", "12987", 5, 5), 2);
  assert_int_equal(br_city("12345", "", 5, 0), 0);
  assert_int_equal(br_city("", "12345", 0, 5), 0);
}

int
main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_brsoundex),
    cmocka_unit_test(test_first_word),
    cmocka_unit_test(test_br_city),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}

