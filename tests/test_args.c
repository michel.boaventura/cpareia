#include "test_args.h"

void
restart_getopt_long() {
  /* Restarts getopt_long, so we can test it multiple times */
  optind = 0;
}

void
test_parse_threads() {
  char *argv[] = {"prog_name", "-t", "10", "-p", "tests/fixtures/foo.xml", 0};
  int argc = 5;
  int max_threads, half_threads;
  args_t *args;

  max_threads = 10;
  half_threads = 4;

  restart_getopt_long();

  args = args_new();
  args_parse(args, argc, argv);

  assert_int_equal(args->max_threads, max_threads);
  assert_int_equal(args->key_threads, half_threads);
  assert_int_equal(args->block_threads, max_threads - half_threads);
  assert_int_equal(args->read_threads, 1);

  args_free(args);
}

void
test_min_threads() {
  char *argv[] = {"prog_name", "-t", "2", "-p", "tests/fixtures/foo.xml", 0};
  int argc = 5;
  int max_threads, half_threads;
  args_t *args;

  restart_getopt_long();

  max_threads = sysconf(_SC_NPROCESSORS_ONLN);
  half_threads = (max_threads - 1) / 2;

  args = args_new();
  args_parse(args, argc, argv);

  assert_int_equal(args->max_threads, max_threads);
  assert_int_equal(args->read_threads, 1);
  assert_int_equal(args->key_threads, half_threads);
  assert_int_equal(args->block_threads, max_threads - half_threads);

  args_free(args);
}

void
test_args() {
  char *argv[] = {"prog_name", "-t", "42", "-p", "tests/fixtures/foo.xml", 0};
  int argc = 5;
  args_t *args;

  restart_getopt_long();

  args = args_new();
  args_parse(args, argc, argv);

  assert_int_equal(args->max_threads, 42);
  assert_string_equal(args->project_file, "tests/fixtures/foo.xml");

  args_free(args);
}

int main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_args),
    cmocka_unit_test(test_min_threads),
    cmocka_unit_test(test_parse_threads),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
