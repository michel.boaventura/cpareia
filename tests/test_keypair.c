#include "test_keypair.h"

void
test_keypair() {
  char key[] = "foo123";
  uint8_t my_rank = 4;

  keypair_t *keypair;

  keypair = keypair_new(1, 10);

  keypair_add_key(keypair, 0, key);

  assert_int_equal(keypair_should_get(keypair, 0, my_rank + 1), 0);
  assert_int_equal(keypair_should_get(keypair, 0, my_rank), 1);
  assert_string_equal(keypair_get_key(keypair, 0), key);

  keypair_free(keypair);
}

void
test_keypair_is_null() {
  char key[] = "\0";

  keypair_t *keypair;

  keypair = keypair_new(1, 10);

  keypair_add_key(keypair, 0, key);

  assert_int_equal(keypair_should_get(keypair, 0, 0), 0);

  keypair_free(keypair);
}

int main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_keypair),
    cmocka_unit_test(test_keypair_is_null),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
