#include "test_project.h"

void
test_project() {
  comparator_t *comparator;
  conjunction_t *conjunction;
  classifier_t *classifier;
  output_t *output;
  float m, u, log2_m_u, log2_1m_1u;
  char *argv[] = {"prog_name", "-t", "10", "-p", "tests/fixtures/foo.xml", 0};
  int argc = 5;

  project_t *project = project_new();

  args_parse(project->args, argc, argv);

  project_parse(project, "tests/fixtures/project.xml");

  assert_string_equal(project->name, "CPareia project");
  assert_string_equal(project->task, "deduplication");

  assert_int_equal(array_size(project->conjunctions), 1);

  conjunction = array_get(project->conjunctions, 0);
  assert_int_equal(array_size(conjunction->parts), 2);

  classifier = project->classifier;
  assert_int_equal(classifier->match_min, 34);
  assert_int_equal(classifier->not_match_max, 20);
  assert_int_equal(array_size(classifier->comparators), 4);

  comparator = array_get(classifier->comparators, 0);
  m = comparator->m;
  u = comparator->u;
  log2_m_u = log2(m / u);
  log2_1m_1u = log2((1 - m) / (1 - u));

  assert_int_equal(comparator->type, approx);
  assert_int_equal(comparator->use_weight_table, 0);
  assert_int_equal(comparator->field, 0);
  assert_in_range(m, 0.24, 0.24);
  assert_in_range(u, 0.2424, 0.2424);
  assert_in_range(comparator->missing, 0, 0);
  assert_string_equal(comparator->function_name, "winkler");
  assert_in_range(comparator->log2_m_u, log2_m_u, log2_m_u);
  assert_in_range(comparator->log2_1m_1u, log2_1m_1u, log2_1m_1u);
  assert_in_range(comparator->default_weight, 1.0, 1.0);
  assert_in_range(comparator->min_value_to_be_match, 0, 0);
  assert_null(comparator->frequency_table_name);

  comparator = array_get(classifier->comparators, 1);
  m = comparator->m;
  u = comparator->u;
  log2_m_u = log2(m / u);
  log2_1m_1u = log2((1 - m) / (1 - u));

  assert_int_equal(comparator->type, approx);
  assert_int_equal(comparator->use_weight_table, 0);
  assert_int_equal(comparator->field, 1);
  assert_in_range(m, 0.4, .24);
  assert_in_range(u, 0.424, 0.424);
  assert_in_range(comparator->missing, 2, 2);
  assert_string_equal(comparator->function_name, "sift4");
  assert_in_range(comparator->log2_m_u, log2_m_u, log2_m_u);
  assert_in_range(comparator->log2_1m_1u, log2_1m_1u, log2_1m_1u);
  assert_in_range(comparator->default_weight, 5.0, 5.0);
  assert_in_range(comparator->min_value_to_be_match, 0, 0);
  assert_null(comparator->frequency_table_name);

  comparator = array_get(classifier->comparators, 2);
  m = comparator->m;
  u = comparator->u;
  log2_m_u = log2(m / u);
  log2_1m_1u = log2((1 - m) / (1 - u));

  assert_int_equal(comparator->type, exact);
  assert_int_equal(comparator->use_weight_table, 0);
  assert_int_equal(comparator->field, 1);
  assert_in_range(m, 0.25, 0.25);
  assert_in_range(u, 0.2525, 0.2525);
  assert_in_range(comparator->missing, 2.3, 2.3);
  assert_string_equal(comparator->function_name, "sift4");
  assert_in_range(comparator->log2_m_u, log2_m_u, log2_m_u);
  assert_in_range(comparator->log2_1m_1u, log2_1m_1u, log2_1m_1u);
  assert_in_range(comparator->default_weight, 1.0, 1.0);
  assert_in_range(comparator->min_value_to_be_match, 0, 0);
  assert_null(comparator->frequency_table_name);

  comparator = array_get(classifier->comparators, 3);
  m = comparator->m;
  u = comparator->u;
  log2_m_u = log2(m / u);
  log2_1m_1u = log2((1 - m) / (1 - u));

  assert_int_equal(comparator->type, brcity);
  assert_int_equal(comparator->use_weight_table, 0);
  assert_int_equal(comparator->field, 1);
  assert_in_range(m, 0.26, 0.26);
  assert_in_range(u, 0.2626, 0.2626);
  assert_in_range(comparator->missing, 2.4, 2.4);
  assert_null(comparator->function_name);
  assert_in_range(comparator->log2_m_u, log2_m_u, log2_m_u);
  assert_in_range(comparator->log2_1m_1u, log2_1m_1u, log2_1m_1u);
  assert_in_range(comparator->default_weight, 2.0, 2.0);
  assert_in_range(comparator->min_value_to_be_match, 0, 0);
  assert_null(comparator->frequency_table_name);

  output = project->output;

  assert_string_equal(output->filename, "/tmp/result");
  assert_in_range(output->min, 500.0, 500.0);
  assert_true(output->max == -500.0);

  project_free(project);
}

int
main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_project),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
